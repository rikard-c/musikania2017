﻿generatePdf.addEventListener("click", function () {


    var students = JSON.parse(getStudents());

    var pdfString =

        "json-input=[{},[\"heading\",\"Time schedule\"],[\"table\", { \"header\": [\"Student name\",\"Start time\"], \"border-width\": 2}," 

    for (var i = 0; i < students.length; i++) {

        pdfString = pdfString +
            "[\"" + students[i].name + "\"," + "\"" + students[i].lessonStartTime + "\"]"

        if (i < students.length - 1) {
            pdfString = pdfString + ",";
        }
    }

    pdfString = pdfString + "]]";

    alert("This string will be sent to the MashApe-API to create a PDF:\n\n"+pdfString);

    var xhttp = new XMLHttpRequest();
    xhttp.withCredentials = true;
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            document.getElementById("test").innerHTML = "Generating PDF......DONE";
            var a = document.createElement('a');
            a.href = window.URL.createObjectURL(xhttp.response); // xhr.response is a blob
            a.download = "Musikania timeschedule.pdf"; // Set the file name.
            a.style.display = 'none';
            document.body.appendChild(a);
            document.getElementById("test").innerHTML = "";
            a.click();
            delete a;
            document.body.style.cursor = "default";
            document.getElementById("send").style.cursor = "default";
        } else if (this.readyState != 4) {
            document.getElementById("test").innerHTML = "Generating PDF......";
            document.body.style.cursor = "progress";
            document.getElementById("send").style.cursor = "progress";
        }
    };
    xhttp.open("POST", "https://yogthos.p.mashape.com/");
    xhttp.responseType = 'blob';
    xhttp.setRequestHeader("x-mashape-key", "PKhdkQ9hoVmsh9X402u4IvRzTJMzp1mCN3Xjsnfin5AZ0kPH6E");
    xhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader("cache-control", "no-cache");
    xhttp.send(pdfString);
});

function getStudents() {

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {

        }
    }
    xhttp.open("GET", "api/schedule/timeschedule", false);
    xhttp.send();
    return xhttp.response;
}
