﻿using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Musikania2017.API.Models;
using Musikania2017.API.Services;
using System.Collections.Generic;

namespace Musikania2017.API.Controllers
{
    [Route("api/lessonsday")]
    public class LessonsDayController : Controller
    {
        private IMusikaniaRepository _musikaniaRepository;

        public LessonsDayController(IMusikaniaRepository musikaniaRepository)
        {
            _musikaniaRepository = musikaniaRepository;
        }

        /// <summary>
        /// Gets the LessonsDays 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("",Name = "GetLessonsDays")]
        public IActionResult GetLessonsDays()
        {
            var lessonsDaysEntities = _musikaniaRepository.GetLessonsDays();

            var results = Mapper.Map<IEnumerable<LessonsDayDto>>(lessonsDaysEntities);

            return Ok(results);
        }

        /// <summary>
        /// Creates a new LessonsDay
        /// </summary>
        /// <param name="lessonsDay"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CreateLessonsDay(    
            [FromBody] LessonsDayForCreationDto lessonsDay)
        {
            if (lessonsDay == null)
            {
                return BadRequest();
            }
            var semester = _musikaniaRepository.GetSemester(lessonsDay.SemesterId);

            if (semester == null)
            {

                return StatusCode(400, $"Invalid semsterId {lessonsDay.SemesterId}");

            }

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var finalLessonsDay = Mapper.Map<Entities.LessonsDay>(lessonsDay);

            _musikaniaRepository.CreateLessonsDay(finalLessonsDay);

            if (!_musikaniaRepository.Save())
            {
                return StatusCode(500, "A proplem happened while handeling your request.");
            }

            var createdLessonsDayToReturn = Mapper.Map<Models.LessonsDayDto>(finalLessonsDay);

            return CreatedAtRoute("GetLessonsDays", new
            { studentId = createdLessonsDayToReturn.Id }, createdLessonsDayToReturn);
        }

        /// <summary>
        /// Deletes a LessonsDay
        /// </summary>
        /// <param name="lessonsDayId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{lessonsDayId}")]
        public IActionResult DeleteLessonsDay(int lessonsDayId)
        {
            if (!_musikaniaRepository.LessonsDayExists(lessonsDayId))
            {
                return NotFound();
            }

            var lessonsDayEntity = _musikaniaRepository.GetLessonsDay(lessonsDayId);

            if (lessonsDayEntity == null)
            {
                return NotFound();
            }

            _musikaniaRepository.DeleteLessonsDay(lessonsDayEntity);

            if (!_musikaniaRepository.Save())
            {
                return StatusCode(500, "A proplem happened while handeling your request.");
            }

            return NoContent();

        }

        /// <summary>
        /// Updates a LessonsDay
        /// </summary>
        /// <param name="lessonsDayId"></param>
        /// <param name="patchDoc"></param>
        /// <returns></returns>
        [HttpPatch]
        [Route("{lessonsDayId}")]
        public IActionResult PartiallyUpdateLessonsDay(int lessonsDayId,
            [FromBody] JsonPatchDocument<LessonsDayForUpdateDto> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            var lessonsDayEntity = _musikaniaRepository.GetLessonsDay(lessonsDayId);
            if (lessonsDayEntity == null)
            {
                return NotFound();
            }

            var lessonsDayToPatch = Mapper.Map<LessonsDayForUpdateDto>(lessonsDayEntity);

            patchDoc.ApplyTo(lessonsDayToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Map(lessonsDayToPatch, lessonsDayEntity);

            if (!_musikaniaRepository.Save())
            {
                return StatusCode(500, "A proplem happened while handeling your request.");
            }

            return NoContent();
        }


    }
}
