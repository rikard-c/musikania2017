﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Musikania2017.API.Models;
using Musikania2017.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Musikania2017.API.Controllers
{

    public class OfferedLessonController : Controller
    {
        private IMusikaniaRepository _musikaniaRepository;

        public OfferedLessonController(IMusikaniaRepository musikaniaRepository)
        {
            _musikaniaRepository = musikaniaRepository;
        }


        /// <summary>
        /// Gets an OfferedLesson for a Student
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/students/{studentId}/offeredlessons")]
        public IActionResult GetOfferedLessonsForStudent(int studentId)
        {
            if (!_musikaniaRepository.StudentExists(studentId))
            {
                return NotFound();
            }

            var offeredLessons = _musikaniaRepository.GetOfferedLessons()
                .Where(s => s.StudentId == studentId);
            var lessonsDays = _musikaniaRepository.GetLessonsDays();

            var foo = from x in offeredLessons
                      join y in lessonsDays
                      on x.LessonsDayId equals y.Id
                      select new
                      {
                          lessonsDayId = y.Id,
                          Lesson = y.Description,
                          y.Date,
                          x.Attended,
                          x.Description,
                      };

            return Ok(foo);
        }

        /// <summary>
        /// Gets a certain OfferedLesson for a Student
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="lessonsDayId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/students/{studentId}/offeredlessons/{lessonsDayId}")]
        public IActionResult GetOfferedLessonForStudent(int studentId, int lessonsDayId)
        {
            if (!_musikaniaRepository.StudentExists(studentId))
            {
                return NotFound();
            }

            return Ok(_musikaniaRepository.GetOfferedLessons()
                .Where(o => o.LessonsDayId == lessonsDayId && o.StudentId == studentId));
        }

        /// <summary>
        /// Gets the OfferedLessons for all Students for a given month in a year
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/students/offeredlessons/{year}/{month}")]
        public IActionResult GetOfferedLessonsForMonth(int year, int month)
        {
            //if (!Enumerable.Range(1, 12).Contains(month))
            //{
            //    return StatusCode(400, "Not a valid month, should be 1 to 12.");
            //}

            var offeredLessons = _musikaniaRepository.GetOfferedLessons();
            var lessonsDays = _musikaniaRepository.GetLessonsDays();
            var students = _musikaniaRepository.GetStudents();

            var foo = from x in offeredLessons
                      join y in lessonsDays
                      on x.LessonsDayId equals y.Id
                      join z in students
                      on x.StudentId equals z.Id
                      where y.Date.Year == year
                      where y.Date.Month == month
                      orderby y.Date
                      select new
                      {
                          Lesson = y.Description,
                          StudentName = z.FirstName +" "+ z.LastName,
                          Date = y.Date.ToString("d"),
                          x.Attended,
                          x.Description,
                      };

            return Ok(foo);

        }

        /// <summary>
        /// Creates an OfferedLesson for a Student
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="offeredLesson"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/students/{studentId}/offeredlessons")]
        public IActionResult CreateOfferedLessonForStudent(int studentId,
            [FromBody] OfferedLessonForCreationDto offeredLesson)
        {
            if (offeredLesson == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (!_musikaniaRepository.StudentExists(studentId))
            {
                return NotFound();
            }

            var offeredLessonEntity = _musikaniaRepository.GetOfferedLessons()
                .FirstOrDefault(o => o.LessonsDayId == offeredLesson.LessonsDayId && o.StudentId == studentId);

            if (offeredLessonEntity != null)
            {
                return StatusCode(400, "An offered lesson for this planned lesson already exists");
            }

            var finalOfferedLesson = Mapper.Map<Entities.OfferedLesson>(offeredLesson);

            _musikaniaRepository.CreateOfferedLesson(studentId, finalOfferedLesson);

            if (!_musikaniaRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            return NoContent();
        }
        /// <summary>
        /// Deletes an OfferedLesson for a Student
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="lessonsDayId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/students/{studentId}/offeredlessons/{lessonsDayId}")]
        public IActionResult DeleteOfferedLesson(int studentId, int lessonsDayId)
        {
            if (!_musikaniaRepository.StudentExists(studentId))
            {
                return NotFound();
            }

            var offeredLessonEntity = _musikaniaRepository.GetOfferedLesson(studentId, lessonsDayId);

            if (offeredLessonEntity == null)
            {
                return NotFound();
            }

            _musikaniaRepository.DeleteOfferedLesson(offeredLessonEntity);

            if (!_musikaniaRepository.Save())
            {
                return StatusCode(500, "A proplem happened while handeling your request.");
            }

            return NoContent();

        }




    }
}
