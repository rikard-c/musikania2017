﻿using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Musikania2017.API.Models;
using Musikania2017.API.Services;
using System.Collections.Generic;
using System.Linq;

namespace Musikania2017.API.Controllers
{
    [Route("api/students")]
    public class StudentController : Controller
    {
        private IMusikaniaRepository _musikaniaRepository;

        public StudentController(IMusikaniaRepository musikaniaRepository)
        {
            _musikaniaRepository = musikaniaRepository;
        }

        /// <summary>
        /// Gets a student by id
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{studentId:int}", Name = "GetStudent")]
        public IActionResult GetStudent(int studentId)
        {
            var studentEntity = _musikaniaRepository.GetStudents()
                .Where(s => s.Id == studentId).FirstOrDefault();

            if (studentEntity == null)
            {
                return NotFound();
            }

            var result = Mapper.Map<StudentDto>(studentEntity);

            return Ok(result);
        }

        /// <summary>
        /// Get Student by FirstName
        /// </summary>
        /// <param name="firstName"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{firstName}", Name = "GetStudentByName")]
        public IActionResult GetStudentByFirstName(string firstName)
        {
            var studentEntity = _musikaniaRepository.GetStudents()
                .Where(s => s.FirstName.ToLower().Contains(firstName.ToLower())).FirstOrDefault();

            if (studentEntity == null)
            {
                return NotFound();
            }

            var result = Mapper.Map<StudentDto>(studentEntity);

            return Ok(result);
        }

        /// <summary>
        /// Gets all Students (optional all sub classes)
        /// </summary>
        /// <param name="allInfo"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetStudents(bool allInfo)
        {
            var studentEntities = _musikaniaRepository.GetStudents();

            if (!allInfo)
            {
                var resultsSimple = Mapper.Map<IEnumerable<StudentDto>>(studentEntities);
                return Ok(resultsSimple);
            }

            var resultsAll = Mapper.Map<IEnumerable<StudentAllInfoDto>>(studentEntities);
            return Ok(resultsAll);

        }

        /// <summary>
        /// Creates a Student
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CreateStudent(
            [FromBody] StudentForCreationDto student)
        {
            if (student == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var finalStudent = Mapper.Map<Entities.Student>(student);

            _musikaniaRepository.CreateStudent(finalStudent);

            if (!_musikaniaRepository.Save())
            {
                return StatusCode(500, "A proplem happened while handeling your request.");
            }

            var createdStudentToReturn = Mapper.Map<Models.StudentDto>(finalStudent);

            return CreatedAtRoute("GetStudent", new
            { studentId = createdStudentToReturn.Id }, createdStudentToReturn);
        }

        /// <summary>
        /// Deletes a Student
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{studentId}")]
        public IActionResult DeleteStudent(int studentId)
        {
            if (!_musikaniaRepository.StudentExists(studentId))
            {
                return NotFound();
            }

            var studentEntity = _musikaniaRepository.GetStudentById(studentId);

            _musikaniaRepository.DeleteStudent(studentEntity);

            if (!_musikaniaRepository.Save())
            {
                return StatusCode(500, "A proplem happened while handeling your request.");
            }

            return NoContent();
        }

        /// <summary>
        /// Partially update a Student
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="patchDoc"></param>
        /// <returns></returns>
        [HttpPatch]
        [Route("{studentId}")]
        public IActionResult PartiallyUpdateStudent(int studentId,    
            [FromBody] JsonPatchDocument<StudentForUpdateDto> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            var studentEntity = _musikaniaRepository.GetStudentById(studentId);
            if (studentEntity == null)
            {
                return NotFound();
            }

            var studentToPatch = Mapper.Map<StudentForUpdateDto>(studentEntity);

            patchDoc.ApplyTo(studentToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Map(studentToPatch, studentEntity);

            if (!_musikaniaRepository.Save())
            {
                return StatusCode(500, "A proplem happened while handeling your request.");
            }

            return NoContent();

        }

        // Test-method, can be deleted    
        // Get: Schedule/ScheduleAsHtml
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("ashtml")]
        public IActionResult Index()
        {
            return View(_musikaniaRepository.GetStudentsForTesting());
        }
    }
}
