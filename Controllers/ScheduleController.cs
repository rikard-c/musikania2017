﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Musikania2017.API.Entities;
using Musikania2017.API.Services;
using System.Linq;
using System.Threading.Tasks;


namespace Musikania2017.API.Controllers
{
    public class ScheduleController : Controller
    {
        private IMusikaniaRepository _musikaniaRepository;
        private readonly MusikaniaContext _context;

        public ScheduleController(IMusikaniaRepository musikaniaRepository, MusikaniaContext context)
        {
            _musikaniaRepository = musikaniaRepository;
            _context = context;
        }

        /// <summary>
        /// Gets a time schedule with the students individual lessons
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/schedule/timeschedule")]
        public IActionResult GetTimeSchedule()
        {
            var result = _musikaniaRepository.GetStudents()
                .Select(s =>
               new
               {
                   Name = $"{s.FirstName} {s.LastName}",
                   LessonStartTime = s.LessonStartTime.ToString("HH:mm"),
                   LessonEndTime = s.LessonEndTime.ToString("HH:mm")
               }).OrderBy(s => s.LessonStartTime);

            return Ok(result);
        }

        /// <summary>
        /// Gets a term schedule for a given semesterId
        /// </summary>
        /// <param name="semesterId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/schedule/termschedule/{semesterId}")]
        public IActionResult GetTermSchedule(int semesterId)
        {
            var semester = _musikaniaRepository.GetSemester(semesterId);

            if (semester == null)
            {
                return NotFound();
            }

            var semesterName = semester.Name;

            var result = _musikaniaRepository.GetLessonsDays()
                .Where(l => l.SemesterId == semesterId)
                .Select(s =>
               new
               {
                   Name = s.Description,
                   Date = s.Date,
                   Semester = semesterName
               });

            return Ok(result);
        }

        //// Get: Schedule
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public IActionResult Index()
        //{
        //    return View(_musikaniaRepository.GetStudentsForTesting());
        //}




        // Test-method, can be deleted    
        // Get: Schedule/ScheduleAsHtml
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("schedule")]
        public IActionResult Index()
        {
            return View(_musikaniaRepository.GetStudentsForTesting());
        }

    }
}
