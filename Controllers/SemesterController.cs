﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Musikania2017.API.Models;
using Musikania2017.API.Services;
using System.Linq;

namespace Musikania2017.API.Controllers
{
    [Route("api/semesters")]
    public class SemesterController : Controller
    {
        private IMusikaniaRepository _musikaniaRepository;

        public SemesterController(IMusikaniaRepository musikaniaRepository)
        {
            _musikaniaRepository = musikaniaRepository;
        }

    /// <summary>
    /// Gets the availiable semesters
    /// </summary>
    /// <returns></returns>
        [HttpGet]
        public IActionResult GetSemesters()
        {
            var result = _musikaniaRepository.GetSemesters();

            if (!result.Any())
            {
                return NotFound();
            }

        return Ok(result);

        }

        /// <summary>
        /// Gets a semester by Id
        /// </summary>
        /// <param name="semesterId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{semesterId}", Name ="GetSemesterById")]
        public IActionResult GetSemesterById(int semesterId)
        {
            var result = _musikaniaRepository.GetSemester(semesterId);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// Creates a new semester
        /// </summary>
        /// <param name="semester"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CreateSemester(
            [FromBody] SemesterForCreationDto semester)
        {
            if (semester == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var finalSemester = Mapper.Map<Entities.Semester>(semester);

            _musikaniaRepository.CreateSemester(finalSemester);

            if (!_musikaniaRepository.Save())
            {
                return StatusCode(500, "A proplem happened while handeling your request.");
            }

            var createdSemesterToReturn = Mapper.Map<Models.SemesterDto>(finalSemester);

            return CreatedAtRoute("GetSemesterById", new
            { semesterId = createdSemesterToReturn.Id }, createdSemesterToReturn);

        }

        /// <summary>
        /// Deletes a semester by Id
        /// </summary>
        /// <param name="semesterId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{semesterId}")]
        public IActionResult DeleteSemester(int semesterId)
        {
            var semester = _musikaniaRepository.GetSemester(semesterId);

            if (semester == null)
            {
                return NotFound();
            }

            _musikaniaRepository.DeleteSemester(semester);

            if (!_musikaniaRepository.Save())
            {
                return StatusCode(500, "A proplem happened while handeling your request.");
            }

            return NoContent();
        }

    }
}
