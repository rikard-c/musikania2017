﻿using Microsoft.AspNetCore.Mvc;

namespace Musikania2017.API.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public IActionResult Index()
        {
            return View();
        }
    }
}
