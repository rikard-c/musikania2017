﻿using Microsoft.AspNetCore.Mvc;
using Musikania2017.API.Models;
using Musikania2017.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Musikania2017.API.Controllers
{
    public class TestController : Controller
    {
        private IMusikaniaRepository _musikaniaRepository;

        public TestController(IMusikaniaRepository musikaniaRepository)
        {
            _musikaniaRepository = musikaniaRepository;
        }

        public IActionResult Index()
        {
            return View(_musikaniaRepository.GetStudentsForTesting());
        }

        public IActionResult Semester()
        {
            return View(_musikaniaRepository.GetSemesters());
        }

        public IActionResult Create()
        {
            return View();
        }

    }
}
