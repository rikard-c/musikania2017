﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Musikania2017.API.Models;
using Musikania2017.API.Services;
using System.Collections.Generic;
using System.Linq;

namespace Musikania2017.API.Controllers
{
    [Route("api/students/{studentId}/guardians")]
    public class GuardianController : Controller
    {
        private IMusikaniaRepository _musikaniaRepository;

        public GuardianController(IMusikaniaRepository musikaniaRepository)
        {
            _musikaniaRepository = musikaniaRepository;
        }

        /// <summary>
        /// Gets a certain Guardian for a Student
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="guardianId"></param>
        /// <returns></returns>
        [HttpGet("{guardianId}", Name = "GetGuardian")]
        public IActionResult GetGuardian(int studentId, int guardianId)
        {
            if (!_musikaniaRepository.StudentExists(studentId))
            {
                return NotFound();
            }

            var guardian = _musikaniaRepository.GetGuardian(studentId, guardianId);

            var guardianResult = Mapper.Map<GuardianDto>(guardian);

            return Ok(guardianResult);
        }

        /// <summary>
        /// Gets all Guardians for a specific Student
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetGuardiansForStudent(int studentId)
        {
            if (!_musikaniaRepository.StudentExists(studentId))
            {
                return NotFound();
            }

            var guardianEntitiesForStudent = _musikaniaRepository.GetGuardians()
                .Where(g => g.StudentId == studentId);

            var guardianEntitiesForStudentToReturn = Mapper.Map<IEnumerable<GuardianDto>>(guardianEntitiesForStudent);

            return Ok(guardianEntitiesForStudentToReturn);
        }

        /// <summary>
        /// Creates a Guardian for a Student
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="guardian"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CreateGuardianForStudent(int studentId,
            [FromBody] GuardianForCreationDto guardian)
        {
            if (guardian == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (!_musikaniaRepository.StudentExists(studentId))
            {
                return NotFound();
            }

            var finalGuardian = Mapper.Map<Entities.Guardian>(guardian);

            _musikaniaRepository.CreateGuardian(studentId, finalGuardian);

            if (!_musikaniaRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            var createdGuardianToReturn = Mapper.Map<Models.GuardianDto>(finalGuardian);

            return CreatedAtRoute("GetGuardian", new
            { StudentId = studentId, guardianId = createdGuardianToReturn.Id }, createdGuardianToReturn);
        }

        /// <summary>
        /// Deletes a Guardian for a Student
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="guardianId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{guardianId}")]
        public IActionResult DeleteGuardian(int studentId, int guardianId)
        {
            if (!_musikaniaRepository.StudentExists(studentId))
            {
                return NotFound();
            }

            var guardianEntity = _musikaniaRepository.GetGuardian(studentId, guardianId);

            if (guardianEntity == null)
            {
                return NotFound();
            }

            _musikaniaRepository.DeleteGuardian(guardianEntity);

            if (!_musikaniaRepository.Save())
            {
                return StatusCode(500, "A proplem happened while handeling your request.");
            }

            return NoContent();

        }
    }
}
