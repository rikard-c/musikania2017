﻿using Musikania2017.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Musikania2017.API
{
    public static class MusikaniaContextExtensions
    {
        public static void EnsureSeedDataForContext(this MusikaniaContext context)
        {
            if (context.Students.Any())
            {
                return;
            }

            var students = new List<Student>()
            {
                new Student()
                {
                    FirstName = "Love",
                    LastName = "Molitor",
                    EmailAddress = "",
                    PhoneNumber = "+46725799091",
                    YearBorn = 2003,
                    LessonStartTime = new DateTime(1,1,1,15,40,0),
                    LessonEndTime = new DateTime(1,1,1,16,05,0),

                    Guardians = new List<Guardian>()
                    {
                        new Guardian()
                        {
                            FirstName = "Ingrid",
                            LastName = "Molitor",
                            EmailAddress = "ingrid.molitor1@gmail.com",
                            PhoneNumber = "+46736796728"
                        },

                        new Guardian()
                        {
                            FirstName = "Lars",
                            LastName = "Molitor",
                            EmailAddress = "lars@wgs.se",
                            PhoneNumber = ""
                        }
                    },
                },

                new Student()
                {
                    FirstName = "Theodor",
                    LastName = "Andrén",
                    EmailAddress = "",
                    PhoneNumber = "",
                    YearBorn = 2005,
                    LessonStartTime = new DateTime(1,1,1,16,05,0),
                    LessonEndTime = new DateTime(1,1,1,16,30,0),

                    Guardians = new List<Guardian>()
                    {
                        new Guardian()
                        {
                            FirstName = "Malin",
                            LastName = "Kernby",
                            EmailAddress = "malin.kernby@wallenstam.se",
                            PhoneNumber = "+46702012220"
                        },

                        new Guardian()
                        {
                            FirstName = "Christoffer",
                            LastName = "Andrén",
                            EmailAddress = "",
                            PhoneNumber = "+46709709818"
                        }
                    },

                },

                new Student()
                {
                    FirstName = "Joel",
                    LastName = "Assow",
                    EmailAddress = "",
                    PhoneNumber = "",
                    YearBorn = 2007,
                    LessonStartTime = new DateTime(1,1,1,16,30,0),
                    LessonEndTime = new DateTime(1,1,1,17,20,0),

                    Guardians = new List<Guardian>()
                    {
                        new Guardian()
                        {
                            FirstName = "Johan",
                            LastName = "Assow",
                            EmailAddress = "johan.assow@smpower.se",
                            PhoneNumber = "+46735324948"
                        }
                    },
                },

                new Student()
                {
                    FirstName = "Doris",
                    LastName = "Pinzke",
                    EmailAddress = "",
                    PhoneNumber = "",
                    YearBorn = 2009,
                    LessonStartTime = new DateTime(1,1,1,17,20,0),
                    LessonEndTime = new DateTime(1,1,1,17,45,0),

                    Guardians = new List<Guardian>()
                    {
                        new Guardian()
                        {
                            FirstName = "",
                            LastName = "Assow",
                            EmailAddress = "niklas.pinzke@nacka.se niklas.pinzke@nacka.se",
                            PhoneNumber = "+46704666698"
                        }
                    },
                },
            };

            var semesters = new List<Semester>()
            {
                new Semester()
                {
                    Name = "VT-17",
                    LessonsDays = new List<LessonsDay>()
                    {
                       new LessonsDay()
                       {
                           Date = new DateTime(2017,01,24),
                           Description = "Lesson 1",
                       },

                        new LessonsDay()
                        {
                            Date = new DateTime(2017,01,31),
                            Description = "Lesson 2",
                        },

                        new LessonsDay()
                        {
                            Date = new DateTime(2017,02,07),
                            Description = "Lesson 3",
                        },

                        new LessonsDay()
                        {
                            Date = new DateTime(2017,02,21),
                            Description = "Lesson 4",
                        }
                    }
                },

                new Semester()
                {
                    Name = "Höstterminen 2017",
                    LessonsDays = new List<LessonsDay>()
                    {
                        new LessonsDay()
                        {
                            Date = new DateTime(2017,09,05),
                            Description = "Första lektionen"
                        },

                        new LessonsDay()
                        {
                            Date = new DateTime(2017,09,12),
                            Description = "Konsert/Clinic med Bootsy Collins"
                        }
                    }
                }
            };

            //var offeredLessons = new List<OfferedLesson>()
            //{
            //    new OfferedLesson()
            //    {
            //        StudentId = 1,
            //        LessonsDayId = 1,
            //        Attended = true,
            //        Description = "Played own song and recorded it in Logic X"
            //    },

            //    new OfferedLesson()
            //    {
            //        StudentId = 1,
            //        LessonsDayId = 2,
            //        Attended = true,
            //        Description = "Played 7 years by Lukas Grayham"
            //    },

            //    new OfferedLesson()
            //    {
            //        StudentId = 2,
            //        LessonsDayId = 1,
            //        Attended = false,
            //        Description = "Didn't show up"
            //    },

            //    new OfferedLesson()
            //    {
            //        StudentId = 2,
            //        LessonsDayId = 2,
            //        Attended = true,
            //        Description = "Played 7 years by Lukas Grayham"
            //    }
            //};

            context.Students.AddRange(students);
            context.Semesters.AddRange(semesters);
            //context.OfferedLessons.AddRange(offeredLessons);
            context.SaveChanges();
        }
    }
}
