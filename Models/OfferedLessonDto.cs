﻿namespace Musikania2017.API.Models
{
    public class OfferedLessonDto
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public bool Attended { get; set; } = false;
    }
}
