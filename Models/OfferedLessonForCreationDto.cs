﻿namespace Musikania2017.API.Models
{
    public class OfferedLessonForCreationDto
    {
        public string Description { get; set; }

        public bool Attended { get; set; } = false;

        public int LessonsDayId { get; set; }
    }
}
