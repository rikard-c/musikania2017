﻿using System;

namespace Musikania2017.API.Models
{
    public class LessonsDayDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int SemesterId { get; set; }
        public DateTime Date { get; set; }
    }
}
