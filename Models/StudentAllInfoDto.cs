﻿using Musikania2017.API.Entities;
using System;
using System.Collections.Generic;

namespace Musikania2017.API.Models
{
    public class StudentAllInfoDto
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public int YearBorn { get; set; }

        public DateTime LessonStartTime { get; set; }

        public DateTime LessonEndTime { get; set; }

        public ICollection<Guardian> Guardians { get; set; }    
            = new List<Guardian>();

        public ICollection<OfferedLesson> OfferedLessons { get; set; }
            = new List<OfferedLesson>();
    }
}
