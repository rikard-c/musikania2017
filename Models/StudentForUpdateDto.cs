﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Musikania2017.API.Models
{
    public class StudentForUpdateDto
    {
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        public int YearBorn { get; set; }
        public string EMailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime LessonStartTime { get; set; }
        public DateTime LessonEndTime { get; set; }
    }
}
