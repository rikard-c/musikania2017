﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Musikania2017.API.Models
{
    public class SemesterForCreationDto
    {
        public string Name { get; set; }
    }
}
