﻿namespace Musikania2017.API.Models
{
    public class GuardianForCreationDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }
    }
}
