﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Musikania2017.API.Models
{
    public class LessonsDayForUpdateDto
    {
        public string Description { get; set; }
        public int SemesterId { get; set; }
        public DateTime Date { get; set; }
    }
}
