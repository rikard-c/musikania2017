﻿using System;

namespace Musikania2017.API.Models
{
    public class StudentDto
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public int YearBorn { get; set; }

        public DateTime LessonStartTime { get; set; }

        public DateTime LessonEndTime { get; set; }
    }
}
