﻿using Musikania2017.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Musikania2017.API.Models
{
    public class SemesterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<LessonsDay> LessonsDays { get; set; }
    }
}
