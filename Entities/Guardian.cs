﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Musikania2017.API.Entities
{
    public class Guardian : Person
    {
        [ForeignKey("StudentId")]
        public int StudentId { get; set; }
    }
}
