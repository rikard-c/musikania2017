﻿namespace Musikania2017.API.Entities
{
    public class OfferedLesson
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //public int Id { get; set; }

        public string Description { get; set; }

        public bool Attended { get; set; } = false;

        //[Key, Column(Order = 0)]
        //[ForeignKey("StudentId")]
        //[InverseProperty("OfferedLessons")]
        //[Column(Order = 0), Key, ForeignKey("StudentId")]
        public int StudentId { get; set; }

        //[Key, Column(Order = 1)]
        //[ForeignKey("LessonsDayId")]
        //[InverseProperty("OfferedLessons")]
        //[Column(Order = 1), Key, ForeignKey("LessonsDayId")]
        public int LessonsDayId { get; set; }
    }
}
