﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Musikania2017.API.Entities
{
    public class LessonsDay
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public string Description { get; set; }

        [ForeignKey("SemesterId")]
        public int SemesterId { get; set; }

        public ICollection<OfferedLesson> OfferedLessons { get; set; }    
            = new List<OfferedLesson>();
    }
}
