﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Musikania2017.API.Entities
{
    public class ModifiedStudent
    {
        public string Name { get; set; }
        public string LessonStartTime { get; set; }
        public string LessonEndTime { get; set; }
        public string EMailAddress { get; set; }
    }
}
