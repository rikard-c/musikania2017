﻿using Microsoft.EntityFrameworkCore;

namespace Musikania2017.API.Entities
{
    public class MusikaniaContext : DbContext
    {

        public DbSet<Student> Students { get; set; }
        public DbSet<Guardian> Guardians { get; set; }
        public DbSet<OfferedLesson> OfferedLessons { get; set; }
        public DbSet<LessonsDay> LessonsDays { get; set; }
        public DbSet<Semester> Semesters { get; set; }

        public MusikaniaContext(DbContextOptions<MusikaniaContext> options)
            : base(options)
        {
            Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OfferedLesson>()
                .HasKey(o => new { o.StudentId, o.LessonsDayId });
        }
    }
}