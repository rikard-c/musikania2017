﻿using System;
using System.Collections.Generic;

namespace Musikania2017.API.Entities
{
    public class Student : Person
    {
        public int YearBorn { get; set; }

        public DateTime LessonStartTime { get; set; }

        public DateTime LessonEndTime { get; set; }
        
        public ICollection<Guardian> Guardians { get; set; }
            = new List<Guardian>();

        public ICollection<OfferedLesson> OfferedLessons { get; set; }
            = new List<OfferedLesson>();
    }
}
