﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Musikania2017.API.Entities;
using Musikania2017.API.Services;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;

namespace Musikania2017.API
{
    public class Startup
    {
        public static IConfigurationRoot Configuration;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appSettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            //Added functionality to be able to use AngularJS with this API
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigins",
                    builder =>
                    {
                        builder.AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin();
                    });
            });

            var connectionString = Startup.Configuration["connectionStrings:musikania2017DBConnectionString"];

            services.AddDbContext<MusikaniaContext>(o => o.UseSqlServer(connectionString));

            services.AddScoped<IMusikaniaRepository, MusikaniaRepository>();

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                //c.TagActionsBy(api => api.HttpMethod);
                //c.OrderActionsBy(a => a.RelativePath);
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Musikania 2017 API",
                    Version = "v1",
                    Description = "Simple Web API for students, parents and the students individual lessons. Created using dotNetCore and EntityFrameworkCore",
                    Contact = new Contact { Name = "Rikard Carlsson" },
                });

                //Set the comments path for the swagger json and ui.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Musikania2017.API.xml");
                c.IncludeXmlComments(xmlPath);
                c.OrderActionsBy((apiDesc) => $"{apiDesc.HttpMethod}");
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,           
            MusikaniaContext musikaniaContext)
        {
            app.UseCors("AllowAllOrigins");

            loggerFactory.AddConsole();

            loggerFactory.AddDebug();

            app.UseStatusCodePages();

            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Entities.Student, Models.StudentDto>();

                cfg.CreateMap<Entities.Student, Models.StudentAllInfoDto>();

                cfg.CreateMap<Entities.Student, Models.StudentForUpdateDto>();

                cfg.CreateMap<Models.StudentDto, Entities.Student>();

                cfg.CreateMap<Models.StudentForCreationDto, Entities.Student>();

                cfg.CreateMap<Models.StudentForUpdateDto, Entities.Student>();


                cfg.CreateMap<Entities.Guardian, Models.GuardianDto>();

                cfg.CreateMap<Entities.Guardian, Models.GuardianForCreationDto>();

                cfg.CreateMap<Models.GuardianForCreationDto, Entities.Guardian>();


                cfg.CreateMap<Entities.LessonsDay, Models.LessonsDayDto>();

                cfg.CreateMap<Models.LessonsDayForCreationDto, Entities.LessonsDay>();

                cfg.CreateMap<Models.LessonsDayForUpdateDto, Entities.LessonsDay>();

                cfg.CreateMap<Entities.LessonsDay, Models.LessonsDayForUpdateDto>();


                cfg.CreateMap<Entities.OfferedLesson, Models.OfferedLessonDto>();

                cfg.CreateMap<Models.OfferedLessonForCreationDto, Entities.OfferedLesson>();


                cfg.CreateMap<Entities.Semester, Models.SemesterForCreationDto>();

                cfg.CreateMap<Models.SemesterForCreationDto, Entities.Semester>();

                cfg.CreateMap<Models.SemesterDto, Entities.Semester>();

                cfg.CreateMap<Entities.Semester, Models.SemesterDto>();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            musikaniaContext.EnsureSeedDataForContext();

            //app.Run(async (context) =>
            //{
            //    await context.Response.WriteAsync("Hello World!");
            //});


            app.UseMvc(
                routes =>
                {
                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}");
                }); 

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Musikania2017.API");
                c.DocExpansion("none");
 
            });

            app.UseStaticFiles();

        }
    }
}
