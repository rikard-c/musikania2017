﻿using System.Collections.Generic;
using System.Linq;
using Musikania2017.API.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace Musikania2017.API.Services
{
    public class MusikaniaRepository : IMusikaniaRepository
    {
        private MusikaniaContext _context;

        //contructor
        public MusikaniaRepository(MusikaniaContext context)
        {
            _context = context;
        }

        //methods
        public void CreateGuardian(int studentId, Guardian guardian)
        {
            var student = _context.Students.FirstOrDefault(s => s.Id == studentId);
            student.Guardians.Add(guardian);
        }
        public void DeleteGuardian(Guardian guardian)
        {
            _context.Guardians.Remove(guardian);
        }

        public void CreateOfferedLesson(int studentId, OfferedLesson offeredLesson)
        {
            var student = _context.Students.FirstOrDefault(s => s.Id == studentId);
            student.OfferedLessons.Add(offeredLesson);
        }
        public void DeleteOfferedLesson(OfferedLesson offeredLesson)
        {
            _context.OfferedLessons.Remove(offeredLesson);
        }

        public void CreateLessonsDay(LessonsDay LessonsDay)
        {
            _context.LessonsDays.Add(LessonsDay);
        }
        public void DeleteLessonsDay(LessonsDay LessonsDay)
        {
            _context.LessonsDays.Remove(LessonsDay);
        }

        public void CreateStudent(Student student)
        {
            _context.Students.Add(student);
        }
        public void DeleteStudent(Student student)
        {
            _context.Students.Remove(student);
        }

        public void CreateSemester(Semester semester)
        {
            _context.Semesters.Add(semester);
        }

        public IEnumerable<Guardian> GetGuardians()
        {
            return _context.Guardians;
        }
        public IQueryable<LessonsDay> GetLessonsDays()
        {
            return _context.LessonsDays;
        }
        public IQueryable<OfferedLesson> GetOfferedLessons()
        {
            return _context.OfferedLessons;
        }
        public IEnumerable<Student> GetStudents()
        {
            return _context.Students
                .Include(s => s.OfferedLessons)
                .Include(s => s.Guardians); 
        }
        public IQueryable<Semester> GetSemesters()
        {
            return _context.Semesters;
        }

        public Guardian GetGuardian(int studentId, int id)
        {
            return _context.Guardians
                .Where(g => g.StudentId == studentId && g.Id == id).FirstOrDefault();
        }
        public OfferedLesson GetOfferedLesson(int studentId, int LessonsDayId)
        {
            return _context.OfferedLessons
                .Where(o => o.StudentId == studentId && o.LessonsDayId == LessonsDayId).FirstOrDefault();
        }
        public LessonsDay GetLessonsDay(int LessonsDayId)
        {
            return _context.LessonsDays.FirstOrDefault(p => p.Id == LessonsDayId);
        }
        public Student GetStudentById(int studentId)
        {
            return _context.Students.FirstOrDefault(s => s.Id == studentId);
        }
        public Semester GetSemester(int semesterId)
        {
            return _context.Semesters.FirstOrDefault(s => s.Id == semesterId);
        }

        public bool StudentExists(int studentId)
        {
            return _context.Students.Any(s => s.Id == studentId);
        }
        public bool LessonsDayExists(int LessonsDayId)
        {
            return _context.LessonsDays.Any(p => p.Id == LessonsDayId);
        }
        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void DeleteSemester(Semester semester)
        {
            _context.Semesters.Remove(semester);
        }

        public IQueryable<ModifiedStudent> GetStudentsForTesting()
        {
            var modifiedStudents = _context.Students
                .Select(s =>
                new ModifiedStudent
                {
                    Name = $"{s.FirstName} {s.LastName}",
                    LessonStartTime = $"{s.LessonStartTime.ToString("HH:mm")}",
                    LessonEndTime = $"{s.LessonEndTime.ToString("HH:mm")}",
                    EMailAddress = s.EmailAddress
                });

            return modifiedStudents;
        }
    }
}
