﻿using Musikania2017.API.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Musikania2017.API.Services
{
    public interface IMusikaniaRepository
    {
        IEnumerable<Student> GetStudents();

        IQueryable<LessonsDay> GetLessonsDays();

        IEnumerable<Guardian> GetGuardians();

        IQueryable<OfferedLesson> GetOfferedLessons();

        IQueryable<Semester> GetSemesters();

        IQueryable<ModifiedStudent> GetStudentsForTesting();

        Student GetStudentById(int studentId);

        Guardian GetGuardian(int studentId, int guardianId);

        OfferedLesson GetOfferedLesson(int studentId, int LessonsDayId);

        LessonsDay GetLessonsDay(int LessonsDayId);

        Semester GetSemester(int semesterId);

        bool StudentExists(int studentId);

        bool LessonsDayExists(int LessonsDayId);

        bool Save();

        void CreateStudent(Student student);

        void DeleteStudent(Student student);

        void CreateLessonsDay(LessonsDay LessonsDay);

        void CreateOfferedLesson(int studentId, OfferedLesson offeredLesson);

        void CreateGuardian(int studentId, Guardian guardian);

        void CreateSemester(Semester semester);

        void DeleteGuardian(Guardian guardian);

        void DeleteOfferedLesson(OfferedLesson offeredLesson);

        void DeleteLessonsDay(LessonsDay LessonsDay);

        void DeleteSemester(Semester semester);
    }
}
