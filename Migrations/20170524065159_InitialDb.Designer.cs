﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Musikania2017.API.Entities;

namespace Musikania2017.API.Migrations
{
    [DbContext(typeof(MusikaniaContext))]
    [Migration("20170524065159_InitialDb")]
    partial class InitialDb
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Musikania2017.API.Entities.Guardian", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("EmailAddress");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("PhoneNumber");

                    b.Property<int>("StudentId");

                    b.HasKey("Id");

                    b.HasIndex("StudentId");

                    b.ToTable("Guardians");
                });

            modelBuilder.Entity("Musikania2017.API.Entities.LessonsDay", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<int>("SemesterId");

                    b.HasKey("Id");

                    b.HasIndex("SemesterId");

                    b.ToTable("LessonsDays");
                });

            modelBuilder.Entity("Musikania2017.API.Entities.OfferedLesson", b =>
                {
                    b.Property<int>("StudentId");

                    b.Property<int>("LessonsDayId");

                    b.Property<bool>("Attended");

                    b.Property<string>("Description");

                    b.HasKey("StudentId", "LessonsDayId");

                    b.HasIndex("LessonsDayId");

                    b.ToTable("OfferedLessons");
                });

            modelBuilder.Entity("Musikania2017.API.Entities.Semester", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Semesters");
                });

            modelBuilder.Entity("Musikania2017.API.Entities.Student", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("EmailAddress");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<DateTime>("LessonEndTime");

                    b.Property<DateTime>("LessonStartTime");

                    b.Property<string>("PhoneNumber");

                    b.Property<int>("YearBorn");

                    b.HasKey("Id");

                    b.ToTable("Students");
                });

            modelBuilder.Entity("Musikania2017.API.Entities.Guardian", b =>
                {
                    b.HasOne("Musikania2017.API.Entities.Student")
                        .WithMany("Guardians")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Musikania2017.API.Entities.LessonsDay", b =>
                {
                    b.HasOne("Musikania2017.API.Entities.Semester")
                        .WithMany("LessonsDays")
                        .HasForeignKey("SemesterId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Musikania2017.API.Entities.OfferedLesson", b =>
                {
                    b.HasOne("Musikania2017.API.Entities.LessonsDay")
                        .WithMany("OfferedLessons")
                        .HasForeignKey("LessonsDayId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Musikania2017.API.Entities.Student")
                        .WithMany("OfferedLessons")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
